fuzzyLenght :: [a] ->String
fuzzyLenght [] = "Empty"
fuzzyLenght [_] = "One"
fuzzyLenght [_,_] = "Two"
fuzzyLenght _ = "Many"


main = do

    print ( fuzzyLenght [] )
    print ( fuzzyLenght [1])
    print ( fuzzyLenght [1,2])
    print ( fuzzyLenght [1, 2])