

myRangeTuple1 :: (Int,Int) -> [Int]
myRangeTuple1 ( a, b) = [a .. b]

myRangeCurry :: Int -> Int -> [Int]
myRangeCurry a b = [a .. b ]


myRangeTuple2 :: Int  -> [Int]
myRangeTuple2 b = myRangeTuple1(0,b)


myRangeCurry2 :: Int -> [Int]
myRangeCurry2 = myRangeCurry 0

myTake2 :: [a] -> [a]
myTake2 = take 2

myGet2 :: [a] -> a
myGet2 =  ( flip (!!)) 2


plus42Positif :: Int -> Bool 
-- plus42Positif x = x + 42 > 0
plus42Positif = (>=0) . (+42)


