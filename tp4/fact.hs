
fact :: Int -> Int
fact a 
    | a > 1 = fact(a - 1) * a 
    | a == 1 = 1


main = do 
    print( fact 2)
    print( fact 3)