loopEcho :: Int -> IO String$
loopEcho 0 = return  "loop terminated"
loopEcho n = do 
    putStr "> "
    input <- getLine
    if input /= ""
        then do
            putStrLn input
            loopEcho (n-1)

        else return "empty line"


main :: IO()
main = do 
    result <- loopEcho 3