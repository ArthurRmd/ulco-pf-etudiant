

mulList :: Int -> [Int] -> [Int]

mulList k [] = []

mulList k (x:xs) = x * k : mulList k xs

selectListe :: ( Int, Int ) -> [Int] -> [Int]
selectListe _ [] = []

selectListe (a,b) (x:xs) =
    if x<=a && x<=b
        then x : selectListe (a,b) xs
        else selectListe (a,b) xs

sumListe :: [Int] -> Int
sumListe [] = 0
sumListe (x:xs) = x + sumListe xs