pgcd :: Int -> Int -> Int
pgcd a b 
    | b == 0 = a
    | otherwise = pgcd b (a `mod` b)