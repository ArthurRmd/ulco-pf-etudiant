
factTco :: Integer -> Integer 
factTco n = aux n 1

    where 
        aux 1 acc = acc 
        aux n acc = aux ( n-1) (acc*n)


main :: IO ()
main = print (factTco 3)