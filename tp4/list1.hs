

myLength :: [a] -> Int

myLength [] = 0
myLength (_:xs) = 1 + myLength xs

myLength' :: [a] -> Int
myLength' l = aux l 0

    where aux [] acc = acc
          aux (_:xs) acc = aux xs (acc+1)  

          

toUpperString :: String -> String
toUpperString "" = "" 
toUpperString (x:xs) = toUpper x : toUpperString xs


onlyLetters :: String -> String 
onlyLetters [] = []
onlyLetters (x:xs) = 
    if isLetter x
        then x : onlyLetters xs
        else onlyLetters xs