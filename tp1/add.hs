
add2Int :: Int -> Int -> Int
add2Int a b = a + b

add2Double ::Double -> Double -> Double
add2Double a b = a + b

add2Num :: Num a => a -> a -> a 
add2Num a b = a + b


u :: Int
u = 3

v  ::Double
v  = 2



main = do 

    print ( add2Int 2 3)
    print ( add2Double 2 3.5)

    print ( add2Num u u )
    print ( add2Num v v  )
    -- print ( add2Num u v )   // u et v ne sont pas du même type



