
main :: IO()

main = do 
    putStrLn "What's your name : "
    
    name <- getLine

    if name /= ""
        
    then do
        putStrLn ("Hello " ++ name ) 
        main
        
    else 
        putStrLn "Good Bye"