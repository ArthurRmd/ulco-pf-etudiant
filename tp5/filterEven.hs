
filterEven1 :: [Int] -> [Int]
filterEven1 [] = []
filterEven1 (x:xs) = 
    if even x
        then x : filterEven1 xs
        else filterEven1 xs


filterEven2 :: [Int] -> [Int]
filterEven2 l = filter even l

myFilter :: ( a -> Bool ) -> [a] ->[a]
myFilter p [] = []
myFilter p (x:xs) =
    if p x
    then x : myFilter p xs 
    else myFilter p xs 