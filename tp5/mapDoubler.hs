
mapDoubler1 :: [Int] -> [Int]
mapDoubler1 [] =  []
mapDoubler1 (x:xs) = x*2 : mapDoubler1 xs

mapDoubler2 :: [Int] -> [Int]
mapDoubler2 x = map (*2) x

mymap :: ( a -> a) -> [a] -> [a]
mymap f [] = []
mymap f (x:xs) = x * f : mymap xs
