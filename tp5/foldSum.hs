
foldSum1 :: [Int] -> Int
foldSum1 [] = 0
foldSum1 ( x:xs) = x + foldlSum1 xs

foldSum2 :: [Int] -> Int
foldSum2 l = foldl (+) 0 l

myfold :: (b -> a -> b) -> b ->[a] -> b
myfold f acc [] =  0
myfold f acc (x:xs) = myfold f ( f acc x) xs