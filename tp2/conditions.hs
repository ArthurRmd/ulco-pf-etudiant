
formaterParite :: Int -> String 

formaterParite x = if even x
    then "pair"
    else "impair"


formaterSigne :: Int -> String

formaterSigne x = if x == 0 
    then "nul"
    else if x < 0
        then "negatif"
        else "positif"

