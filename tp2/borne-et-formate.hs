borneEtFormate1 :: Double -> String

borneEtFormate1 x = 
    show x ++ "-> " ++ show x' 
    where x' = 
            if x < 0
            then 0
            else if x > 1
                then 1
                else x


    
main :: IO()
main = do 
    putStrLn (borneEtFormate1 0.2)
    putStrLn (borneEtFormate1 2.1)
    putStrLn (borneEtFormate1 0.2)
    putStrLn (borneEtFormate1 0.2)