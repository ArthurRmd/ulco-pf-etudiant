
fibo :: Int -> Int


fibo n =  
    case n of
        0 -> 0 
        1 -> 1
        _  -> fibo (n-1) + fibo(n-2)


main = do
    print (fibo 100 )