import Text.Read( readMaybe)


safeSqrt :: Ouble -> Maybe Double 
safeSqrt x = 
    if x > 0 
        then Just(sqrt x)
        else Nothing

main = do
    print ( safeSqrt 16)
    print (safeSqrt (-1))
    str <- getLine
    let x = readMaybestr :: Maybe Int
    print x


